# linkcheck

Dockerized [linkcheck](https://github.com/filiph/linkcheck )

### Usage

```
docker run registry.gitlab.com/greg/linkcheck <URL>
```
